$( document ).ready(function() {
    var searchKeyWord, url, pages = [], searchResultHtml = '', randomPageId;

    $('#submitKeyword').on('click', function () {
        pages = [];
        $('#searchResult').empty();
        searchResultHtml = '';

        searchKeyWord = $('#wikipediaKeyWord').val();
        url = 'https://en.wikipedia.org/w/api.php?format=json&action=query&generator=search&gsrsearch=' + searchKeyWord + '&prop=extracts&exintro&explaintext&exsentences=1&exlimit=max&callback=?';

        $.getJSON(url, function (data) {

            for (var index in data.query.pages) {
                pages.push(data.query.pages[index]);
            }

            for (var index=0; index<pages.length; index++) {
                var redirectURL = 'https://en.wikipedia.org/?curid=' + pages[index].pageid;
                console.log(redirectURL);
                searchResultHtml += ('<div class="card text-center">' +
                    '<div class="card-block">' +
                    '<h4 class="card-title">'+ '<a href=' + redirectURL + ' target="_blank">' +  pages[index].title + '</a></h4>' +
                    '<p class="card-text">' + pages[index].extract + '</p> </div> </div> <br/>');
                $('#searchResult').html(searchResultHtml);
            }
        });
    });

    $('#randomPage').on('click', function () {
        url = "https://en.wikipedia.org/w/api.php?action=query&&format=json&list=random&rnlimit=1&callback=?";
        $.getJSON(url, function (data) {
            randomPageId = data.query.random[0].id;
            window.open('https://en.wikipedia.org/?curid=' + randomPageId, '_self');
        });
    });
    
});